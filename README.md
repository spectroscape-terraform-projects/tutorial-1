# Tutorial 1: Deploy a simple nginx container in Docker using Terraform

This tutorial is based on [this hashicorp tutorial](https://developer.hashicorp.com/terraform/tutorials/docker-get-started/infrastructure-as-code), but instruction have been added to complete begineer.

> I assume that you are familiar with your system terminal.

### In order for this project to work, the following tools need to be installed on your system:
  - Docker
  - Terraform

## Installation

Firstly, check your linux distribution by typing in a terminal

    cat /etc/os-release

> For this example, I will assume that you are using an arch based linux distribution with systemd and sudo installed.

<br>
Install docker and terraform this way:

    sudo pacman -Syy docker terraform

then start the docker daemon by typing

    sudo systemctl enable --now docker

finally, create a docker group and add yourself in it

    sudo groupadd docker
    sudo usermod -aG docker $(whoami)

> Note: it is possible to add terraform autocompletion if you are using bash or zsh as shell terminal with `terraform -install-autocomplete`.
It is also possible to modify the docker daemon ip range (if you are using docker in a virtual machine on a docker bridge with the default ip range).


## Running this tutorial


Proceed to type:

    terraform init

Terraform will make use of the `main.tf`, an hcl file written to use docker as the provider for this orchestration.
Apply the infrastructure described by executing:


    terraform apply

and then type `yes`.

Now, the container can be seen running by typing:


    docker ps

> We can see that out nginx container is running

<br>
Finally, to destroy the nginx instance proceed as followed:


    terraform destroy

and then type `yes`.

Our infrastructure is now destroyed, as no container can be seen running, paused or stopped by typing


    docker ps -a
