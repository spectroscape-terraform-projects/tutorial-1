#Terraform settings
terraform {
  required_providers {
    docker = {
      #for each profider, the source provider define an optional hostname, a namespace and a the provider type.
      source  = "kreuzwerker/docker" #shorthand for registry.terraform.io/kreuzwerker/docker
      version = "~> 3.0.2"          #optional attribute, but recommended.
    }
  }
}

provider "docker" {}

#A resource is a physical or virtual component of our infrastructure.
resource "docker_image" "nginx" { #ressource type and name: it forms the id "docker_image.nginx"
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.image_id #or .name
  name  = "tutorial"

  ports {
    internal = 80
    external = 8080
  }
}
